import pixelpainter.PixelPainter;

import java.awt.*;

public class Game {

    public static void main(String[] args) {

        EventQueue.invokeLater(new PixelPainter());
    }
}