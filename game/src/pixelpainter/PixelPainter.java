package pixelpainter;

import javax.sound.sampled.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.*;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Random;

public class PixelPainter implements Runnable {

    // Victory? Start with false in case of early save
    boolean win = false;

    // Score
    public static int playerScore = 0, playerLevel = 1;

    // File
    public File fileToSave;

    // Frame and Panel
    private JFrame frame;
    private JPanel pixelPanel;

    // Width, Height
    private int width, height;

    // Preferred Width, Preferred Height
    private int preferredWidth, preferredHeight;

    // Save Panel, Save Width, Save Height
    private JPanel savedPanel;
    private int savedWidth = 0, savedHeight = 0;

    // Start Panel, Start Width, Start Height
    private JPanel startPanel;
    private int startWidth = 0, startHeight = 0;

    // Username
    public static String username = "";

    // Default Constructor
    public PixelPainter() {}

    // Implements methods in Runnable
    @Override
    public void run() {

        initGUI();
        setUsername();
        userHelp();
    }

    // initGUI ( default - no parameters )
    private void initGUI() {

        // Create new frame
        frame = new JFrame("Pixel Art");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Add pixels to our pixelPanel
        pixelPanel = createPixels();

        // Initialize starting panel, width and height
        startPanel = pixelPanel;
        startWidth = startWidth + width;
        startHeight = startHeight + width;

        // Add the panel to frame and create Menu Bar
        frame.add(pixelPanel);
        createMenuBar(pixelPanel);

        // Pack (Sort) it, set it's location and visibility
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    // initGUI with parameter panel
    private void initGUI(JPanel panel) {

        // Create new frame
        frame = new JFrame("Pixel Art");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Initialize the pixelPanel with the parameter panel
        this.pixelPanel = panel;

        // Add the panel to frame and create Menu Bar
        frame.add(pixelPanel);
        createMenuBar(pixelPanel);

        // Pack (Sort) it, set it's location and visibility
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    // initGUI with parameters preferred width and preferred height
    private void initGUI(int prefWidth, int prefHeight) {

        // Create new frame
        frame = new JFrame("Pixel Art");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Initialize the preferredWidth and preferredHeight to our parameters
        this.preferredWidth = prefWidth;
        this.preferredHeight = prefHeight;

        // Add pixels to panel with the prefWidth and prefHeight
        pixelPanel = createPixels(prefWidth, prefHeight);

        // Add the panel to frame and create Menu Bar
        frame.add(pixelPanel);
        createMenuBar(pixelPanel);

        // Pack (Sort) it, set it's location and visibility
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    // createPixels ( default - no parameters )
    private JPanel createPixels() {

        // Create random object from Random class
        Random random = new Random();

        // This generates the range between 0 - 48, add another 1 to id to make it 1 - 49
        width = random.nextInt(49) + 1;
        height = random.nextInt(49) + 1;

        // Create new JPanel
        JPanel panel = new JPanel();

        // Set the layout to GridLayout with parameters height, width
        panel.setLayout(new GridLayout(height, width, 0, 0));

        // Iterate through
        for (int row = 0; row < height; row++) {
            for (int column = 0; column < width; column++) {

                // Creating object from the PixelPanel class
                PixelPanel pixelPanel = new PixelPanel();

                // Adding the mouseListener that is the new ColorListener from pixelPanel
                pixelPanel.addMouseListener(new ColorListener(pixelPanel));

                // Add the pixelPanel to panel
                panel.add(pixelPanel);
            }
        }

        return panel;
    }

    // createPixels with parameters set width and set height for custom new
    private JPanel createPixels(int setWidth, int setHeight) {

        // Connecting constructor variables with our variables
        this.width = setWidth;
        this.height = setHeight;

        // New JPanel
        JPanel panel = new JPanel();

        // Setting Gridlayout with our constructors integers
        panel.setLayout(new GridLayout(setWidth, setHeight, 0, 0));

        // Iterate through
        for (int row = 0; row < setHeight; row++) {
            for (int column = 0; column < setWidth; column++) {

                // Creating object from the PixelPanel class
                PixelPanel pixelPanel = new PixelPanel();

                // Adding the mouseListener that is the new ColorListener from pixelPanel
                pixelPanel.addMouseListener(new ColorListener(pixelPanel));

                // Add the pixelPanel to panel
                panel.add(pixelPanel);
            }
        }

        return panel;
    }

//  Methods found in file menu

    // newGame method
    private void newGame() {

        // Standard output for verification
        System.out.println("Relaunching pixels for a new game!");

        // Close the old frame
        frame.dispose();

        // Resetting the score
        playerScore = 0;

        // InitGUI without parameters, the default randomized one
        initGUI();

        // Standard output for verification
        System.out.println("Board reshuffled with new pixels! Good luck!");
    }

    // newPreferredGame method
    private void newPreferredGame() {

        // Get user input
        preferredWidth = Integer.parseInt(JOptionPane.showInputDialog(null, "Enter columns: (less than 50!)"));
        preferredHeight = Integer.parseInt(JOptionPane.showInputDialog(null, "Enter rows: (less than 50!)"));

        // Condition to meet according to Game Rules
        if (preferredWidth < 50 && preferredHeight < 50 && preferredWidth > 0 && preferredHeight > 0) {

            // Closing old frame
            frame.dispose();

            // Standard output verification
            System.out.println("Successfully created preferred size of matrix M x N");

            // Resetting the score
            playerScore = 0;

            // InitGUI with 2 parameters - preferredWidth and preferredHeight parameters
            initGUI(preferredWidth, preferredHeight);

        // In case user doesn't follow constraints, take the input again, prompt it twice, if constraints do not meet, do nothing
        }else {

            // Standard output for verification
            System.out.println("Error! Stick to constraints!");

            // Take user input again through JOptionPane showInputDialog method
            preferredWidth = Integer.parseInt(JOptionPane.showInputDialog(null, "Sorry, constraints! Trying again before disposing! Insert columns: (less than 50!)"));
            preferredHeight = Integer.parseInt(JOptionPane.showInputDialog(null, "OptionPane will dispose unless you are in constraints. Insert rows: (less than 50!)"));

            // Standard output for verification
            System.out.println("Unsuccessful! Constraints..");
        }
    }

    // restartGame method for starting panel
    private void restartGame() {

        // Close the old frame
        frame.dispose();

        // Reset the score
        playerScore = 0;

        // InitGUI with parameter - startPanel parameter
        initGUI(startPanel);
    }

    // saveFile method
    private void saveFile() {

        JOptionPane.showMessageDialog(null,"Please save the file with extension .txt, so you can review it later ;)", "Save Format", JOptionPane.INFORMATION_MESSAGE);

        // Creating new fileChooser from JFileChooser class
        JFileChooser fileChooser = new JFileChooser();

        // Adding OpenDialog to the parent frame null
        fileChooser.setDialogTitle("Save To: ");

        // For handling when user pressed save or cancel
        int userSelection = fileChooser.showSaveDialog(null);

        // Condition what to do in case user clicks open
        if(userSelection == JFileChooser.APPROVE_OPTION) {

            // Get the selected file in variable fileToSave
            fileToSave = fileChooser.getSelectedFile();

            try {

                // Informing the user
                FileWriter fw = new FileWriter(fileToSave);

                // Date and Time formatter
                DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");

                // Pulling current time using LocalTime class
                LocalTime localTime = LocalTime.now();

                // Adding it to string theTime
                String theTime = dateTimeFormatter.format(localTime);

                // Adding the time in the string to handle save file output in .txt file
                String userTime = "\tLocal time for session was: " + theTime + "\n\n";

                // Adding values of panel and dimension to new objects with prefix 'saved'
                savedPanel = pixelPanel;
                savedWidth = savedWidth + width;
                savedHeight = savedHeight + width;

                // Append to save file
                fw.append("\t\tPixelPainter Data\n\n");
                fw.append("\tUsername: ").append(username).append("\n");
                fw.append("\tLevel: ").append(String.valueOf(playerLevel)).append("\n");
                fw.append("\tScore: ").append(String.valueOf(playerScore)).append("\n");
                fw.append("\tMatrix size M x N: ").append(String.valueOf(width)).append(" x ").append(String.valueOf(height));
                fw.append(userTime);
                fw.append("\tCreated by: Hadzic Dino");

                // Closing FileWriter properly
                fw.flush();
                fw.close();
            } catch (IOException e) {

                e.printStackTrace();
            }

            // Standard output for verification
            System.out.println("Successful Save! TEST");
        }
    }

    // openSaved method
    private void openSaved () {

        // Informing the user
        JOptionPane.showMessageDialog(null,"You will get prompt twice to open, in case you change your mind after clicking open!", "Open Handler Notice", JOptionPane.INFORMATION_MESSAGE);

        // Creating new fileChooser from JFileChooser class
        JFileChooser fileChooser = new JFileChooser();

        // Adding OpenDialog to the parent frame null
        fileChooser.showOpenDialog(null);

        // Setting the dialog title
        fileChooser.setDialogTitle("Open: ");

        // For handling when user pressed open or cancel
        int userSelection = fileChooser.showOpenDialog(null);

        // Condition what to do in case user clicks open
        if(userSelection == JFileChooser.APPROVE_OPTION) {

            // Close the old frame
            frame.dispose();

            // InitGUI with one parameter - the JPanel parameter
            initGUI(savedPanel);
        }
    }

    // userHelp method
    private void userHelp() {

        // All rules to one string to handle showMessageDialog method
        String welcome = "GAME RULES:\n" +
                "The game takes place in the field of size n × m, where m and n are less than 50. \n" +
                "Each cell contains a single color or character or number (your choice) from a set of possible values. \n" +
                "The program is defined by three parameters n, m, k. The program randomly places \n" +
                "characters / colors / numbers on a two-dimensional field. The first \n" +
                "color / character / number belongs to the player. The object of the game is to have the player paint \n" +
                "the entire playing field with their color. Coloring is enabled by selecting the source field \n" +
                "and the target field. The game replaces all adjacent fields with the target color if they are \n" +
                "the same color as the target field in the source field color. The game shows the user a list \n" +
                "of colors / characters / numbers. When the user paints the entire field, the game ends and the \n" +
                "number of moves is displayed.\n" +
                "The goal of the game is to reach level 10 and win. If you go to negative (under 0) you will lose the game!\n" +
                "With each pixel painted you get rewarded with 5 points! But when you select a color you lose 10!!!\n" +
                "When you gather 20 points you gain a level and your points get reset to 0. \n" +
                "You start with 1 level. NOTE: With loss of 20 points you also lose a level!!!";

        // Informing the user
        JOptionPane.showMessageDialog(null, welcome, "Welcome To PixelPainter!", JOptionPane.INFORMATION_MESSAGE);
    }

// Methods found in stats menu
    // userScore method
    private void userScore() {

        // Another string to handle JOptionPane showMessageDialog method
        String userScore = "Your score is: " + playerScore;

        // Informing the user
        JOptionPane.showMessageDialog(null, userScore, "Your Score", JOptionPane.INFORMATION_MESSAGE);
    }

    // localTime method
    private void localTime() {

        // Date and Time formatter
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");

        // Pulling current time using LocalTime class
        LocalTime localTime = LocalTime.now();

        // Adding it to string theTime
        String theTime = dateTimeFormatter.format(localTime);

        // Another string to handle JOptionPane showMessageDialog method
        String userTime = "Local time is: " + theTime;

        // Informing the user
        JOptionPane.showMessageDialog(null, userTime, "Local Time", JOptionPane.INFORMATION_MESSAGE);
    }

    // Set username
    private void setUsername() {

        // Taking input with JOptionPane class
        username = JOptionPane.showInputDialog(frame, "Choose username:");
    }

    // Get username
    private void getUsername() {

        // Informing the user
        JOptionPane.showMessageDialog(null, "Greetings " + username + "!", "Your username!", JOptionPane.INFORMATION_MESSAGE);
    }

    // Level Up
    private void lvlUp() {

        // Setting playerScore back to 0
        playerScore = 0;

        // Informing the user
        JOptionPane.showMessageDialog(null, "Congratulations " + username + "! You have leveled UP!", "Level UP!", JOptionPane.INFORMATION_MESSAGE);

        // Incrementing level by 1
        playerLevel++;
    }

    // Level Down - Surprise!
    private void lvlDown() {

        // Setting playerScore back to 0
        playerScore = 0;

        // Informing the user
        JOptionPane.showMessageDialog(null, "Unfortunately you lost 20 points and 1 level. Use help for more! Check your level in 'Edit' menu!", "Level DOWN!", JOptionPane.INFORMATION_MESSAGE);

        // Decrementing level by 1
        playerLevel--;
    }

    // Getter for level
    private void getLevel() {

        // Informing the user of his/hers level
        JOptionPane.showMessageDialog(null, "Your level is: " + playerLevel, "Your level!", JOptionPane.INFORMATION_MESSAGE);
    }

    // Simple method to play .wav files using Clip class
    public void playMusic(String audioFilePath) {

        try{

            // Creating object Clip and calling AudioSystem.getClip() method
            Clip clip = AudioSystem.getClip();

            // Using object to get the new AudioInputStream File - audioFilePath parameter
            clip.open(AudioSystem.getAudioInputStream(new File(audioFilePath)));

            // Start the song
            clip.start();
        }catch (Exception e){

            e.printStackTrace(System.out);
        }
    }

    // Method used when archiving 10-th level and winning the Game
    private void saveWin () {

        try {

            // Modify the path to your pc
            FileWriter fw = new FileWriter("/home/jim/IdeaProjects/OOP - PixelPainter Game/src/saves/Victory_" + username + ".txt");

            // Creating Date and Time Formatter
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");

            // Pulling current time through LocalTime class
            LocalTime localTime = LocalTime.now();

            // Assigning the date to string 'theTime'
            String theTime = dateTimeFormatter.format(localTime);

            // Another string, this one will get appended to the save file
            String userTime = "\tLocal time for session was: " + theTime + "\n\n";

            // Append to save file
            fw.append("\t\tPixelPainter Data\n\n");
            fw.append("\tUsername: ").append(username).append("\n");
            fw.append("\tLevel: ").append(String.valueOf(playerLevel)).append("\n");
            fw.append("\tScore: ").append(String.valueOf(playerScore)).append("\n");
            fw.append("\tMatrix size M x N: ").append(String.valueOf(width)).append(" x ").append(String.valueOf(height)).append("\n");
            fw.append("\tVictory: ").append(String.valueOf(win)).append("\n");
            fw.append("\tGood job on winning! You rocked it!\n");
            fw.append(userTime);
            fw.append("\tCreated by: Hadzic Dino");

            // Closing FileWriter properly
            fw.flush();
            fw.close();
        } catch (IOException e) {

            e.printStackTrace();
        }
    }

    // createMenuBar with parameter pixelPanel
    private void createMenuBar(JPanel pixelPanel) {

        // Declaration of MenuBar var
        var menuBar = new JMenuBar();

        // Declarations for icons on File menu
        var iconNew = new ImageIcon("src/resources/new.png");
        var iconNewPreferred = new ImageIcon("src/resources/newp.png");
        var iconSave = new ImageIcon("src/resources/save.png");
        var iconOpen = new ImageIcon("src/resources/open.png");
        var iconRestart = new ImageIcon("src/resources/restart.png");
        var helpIcon = new ImageIcon("src/resources/restart.png");
        var exitIcon = new ImageIcon("src/resources/exit.png");

        // Declarations for icons on Edit menu
        var statsIcon = new ImageIcon("src/resources/stats.png");
        var levelIcon = new ImageIcon ("/src/resources/level.png");
        var localTimeIcon = new ImageIcon ("/src/resources/stats.png");
        var usernameIcon = new ImageIcon ("/src/resources/username.png");

        // Our File Menu
        var fileMenu = new JMenu("File");

        // Our Edit Menu
        var editMenu = new JMenu("Edit");

        // Start Over ( New Game )
        var newMenuItem = new JMenuItem("New", iconNew);
        newMenuItem.setMnemonic(KeyEvent.VK_N);
        newMenuItem.setToolTipText("New Game");
        newMenuItem.addActionListener((event) -> newGame());

        // Start Over ( New Game ) with preferred matrix size
        var newPreferredMenuItem = new JMenuItem("Custom New", iconNewPreferred);
        newPreferredMenuItem.setMnemonic(KeyEvent.VK_C);
        newPreferredMenuItem.addActionListener((event) -> newPreferredGame());

        // Restart Game
        var restartMenuItem = new JMenuItem("Restart New", iconRestart);
        restartMenuItem.setMnemonic(KeyEvent.VK_R);
        restartMenuItem.setToolTipText("Restart New");
        restartMenuItem.addActionListener((event) -> restartGame());

        // Save Progress
        var saveMenuItem = new JMenuItem("Save", iconSave);
        saveMenuItem.setMnemonic(KeyEvent.VK_S);
        saveMenuItem.setToolTipText("Save Progress");
        saveMenuItem.addActionListener((event) -> saveFile());

        // Open Progress
        var openMenuItem = new JMenuItem("Open", iconOpen);
        openMenuItem.setMnemonic(KeyEvent.VK_O);
        openMenuItem.setToolTipText("Open Saved");
        openMenuItem.addActionListener((event) -> openSaved());

        // Open Help Dialog
        var helpMenuItem = new JMenuItem ("Help", helpIcon);
        helpMenuItem.setMnemonic(KeyEvent.VK_H);
        helpMenuItem.setToolTipText("Help");
        helpMenuItem.addActionListener((event) -> userHelp());

        // Exit the application
        var eMenuItem = new JMenuItem("Exit", exitIcon);
        eMenuItem.setMnemonic(KeyEvent.VK_E);
        eMenuItem.setToolTipText("Exit application");
        eMenuItem.addActionListener((event) -> System.exit(0));

        // Add menu items to fileMenu
        fileMenu.add(newMenuItem);
        fileMenu.add(newPreferredMenuItem);
        fileMenu.add(restartMenuItem);
        fileMenu.add(saveMenuItem);
        fileMenu.add(openMenuItem);
        fileMenu.add(helpMenuItem);
        fileMenu.add(eMenuItem);

        // Add fileMenu to menuBar
        menuBar.add(fileMenu);

        // Score
        var scoreMenuItem = new JMenuItem ("Score", statsIcon);
        scoreMenuItem.setMnemonic(KeyEvent.VK_S);
        scoreMenuItem.setToolTipText("Score");
        scoreMenuItem.addActionListener((event) -> userScore());

        // Level
        var levelMenuItem = new JMenuItem ("Level", levelIcon);
        levelMenuItem.setMnemonic(KeyEvent.VK_L);
        levelMenuItem.setToolTipText("Level");
        levelMenuItem.addActionListener((event) -> getLevel());

        // Local time
        var localTimeMenuItem = new JMenuItem("Local Time", localTimeIcon);
        localTimeMenuItem.setMnemonic(KeyEvent.VK_L);
        localTimeMenuItem.setToolTipText("Local Time");
        localTimeMenuItem.addActionListener((event) -> localTime());

        // Username
        var usernameMenuItem = new JMenuItem("Username", usernameIcon);
        usernameMenuItem.setMnemonic(KeyEvent.VK_U);
        usernameMenuItem.setToolTipText("Username");
        usernameMenuItem.addActionListener((event) -> getUsername());

        // Add menu item to editMenu
        editMenu.add(scoreMenuItem);
        editMenu.add(levelMenuItem);
        editMenu.add(localTimeMenuItem);
        editMenu.add(usernameMenuItem);

        // Add editMenu to menuBar
        menuBar.add(editMenu);

        // Set it to the frame
        frame.setJMenuBar(menuBar);
    }

    // MouseAdapter and our ColorListener. This class contains the main action event Left/Right click
    public class ColorListener extends MouseAdapter {

        // Panel
        private PixelPanel panel;

        // Color
        private Color selectedColor = Color.BLACK;

        // Constructor
        public ColorListener(PixelPanel panel) {
            this.panel = panel;
        }

        // Our Mouse Listener and main action event
        @Override
        public void mousePressed(MouseEvent event) {

            // Left click paints, while Right click gets the color from pixels
            if (event.getButton() == MouseEvent.BUTTON1) {

                panel.setBackgroundColor(selectedColor);
                panel.repaint();
                playerScore += 5;
            }else if (event.getButton() == MouseEvent.BUTTON3) {

                selectedColor = panel.getBackgroundColor();
                playerScore -= 10;
            }

            // Reward/Punish conditions
            if (playerScore == 20) {

                lvlUp();
            }else if (playerScore == -20) {

                lvlDown();
            }

            // Lose the Game
            if (playerLevel < 0) {

                // Informing the user
                JOptionPane.showMessageDialog(null, "Unfortunately, your level went negative and you lost the game.Better luck next time " + username + "!", "GAME OVER!", JOptionPane.INFORMATION_MESSAGE);

                // Put the path to the .wav file here
                String loseMusic = "/home/jim/IdeaProjects/OOP - PixelPainter Game/src/sounds/fail.wav";

                // Informing the user
                JOptionPane.showMessageDialog(null, "Do not interrupt the message, the game will continue shortly!", "Notification!", JOptionPane.INFORMATION_MESSAGE);

                // Call playMusic with parameter loseMusic which is our path to the .wav file
                playMusic(loseMusic);

                // Using Thread class we call sleep in duration of 8 seconds respectively towards the length of .wav file
                try{
                    Thread.sleep(8000);
                }catch (InterruptedException e){

                    e.printStackTrace(System.out);
                }

                // Taking input from the user using JOptionPane class, showInputDialog method
                String continuePlaying = JOptionPane.showInputDialog(frame, "Do you wish to play again? (YES or NO): - Not case sensitive!");

                // Handling capital letters by converting the whole string to lower case
                String userChoice = continuePlaying.toLowerCase();

                if (userChoice.equals("yes")) {

                    playerScore = 0;
                    playerLevel = 1;
                    username = "";
                    frame.dispose();
                    run();

                }else if (userChoice.equals("no")) {

                    // Informing the user
                    JOptionPane.showMessageDialog(null, "The game will automatically close after you close this message!", "Visit Us Again Soon!", JOptionPane.INFORMATION_MESSAGE);
                    // Exit the application
                    System.exit(0);
                }else {

                    // Informing the user
                    JOptionPane.showMessageDialog(null, "Unclear input, the application will now exit!", "Bad Input!", JOptionPane.INFORMATION_MESSAGE);
                    // Exit the application
                    System.exit(0);
                }

            // Win the Game
            }else if (playerLevel == 10) {

                // Informing the user
                JOptionPane.showMessageDialog(null, "Congratulations! You won the game. Marvelous moves " + username + "!", "Victory!", JOptionPane.INFORMATION_MESSAGE);

                // Set the boolean win variable to true
                win = true;

                // Calling the saveWin function to handle our file without user having to
                saveWin();
                // Informing the user
                JOptionPane.showMessageDialog(null, "Game Saved in the project folder 'saves'. Find it there!", "Win Recorded!", JOptionPane.INFORMATION_MESSAGE);

                // Path to .wav file here
                String victoryMusic = "/home/jim/IdeaProjects/OOP - PixelPainter Game/src/sounds/win.wav";

                // Informing the user
                JOptionPane.showMessageDialog(null, "Do not interrupt the message, the game will continue shortly!", "Notification!", JOptionPane.INFORMATION_MESSAGE);
                playMusic(victoryMusic);

                // Using Thread class we call sleep in duration of 8 seconds respectively towards the length of .wav file
                try{
                    Thread.sleep(8000);
                }catch (InterruptedException e){

                    e.printStackTrace(System.out);
                }

                // Taking input from the user using JOptionPane class, showInputDialog method
                String continuePlaying = JOptionPane.showInputDialog(frame, "Do you wish to play again? (YES or NO): - Not case sensitive!");

                // Handling capital letters by converting the whole string to lower case
                String userChoice = continuePlaying.toLowerCase();

                if (userChoice.equals("yes")) {

                    playerScore = 0;
                    playerLevel = 1;
                    username = "";
                    frame.dispose();
                    run();

                }else if (userChoice.equals("no")) {

                    // Informing the user
                    JOptionPane.showMessageDialog(null, "The game will automatically close after you close this message!", "Visit Us Again Soon!", JOptionPane.INFORMATION_MESSAGE);
                    // Exit the application
                    System.exit(0);
                }else {

                    // Informing the user
                    JOptionPane.showMessageDialog(null, "Unclear input, the application will now exit!", "Bad Input!", JOptionPane.INFORMATION_MESSAGE);
                    // Exit the application
                    System.exit(0);
                }
            }
        }
    }
}