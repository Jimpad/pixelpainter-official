package pixelpainter;

import javax.swing.*;
import java.awt.*;

public class PixelPanel extends JPanel {

    // Here we have final value of pixelSize set
    private static final int pixelSize = 50;

    private Color backgroundColor;

    // Adding randoms, border and preferred size
    public PixelPanel() {

        // Adding random colors, black lineBorder, and preferred size
        this.backgroundColor = new Color((int)(Math.random() * 0x1000000));
        this.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        this.setPreferredSize(new Dimension(pixelSize, pixelSize));
    }

    // Get for Background
    public Color getBackgroundColor() {
        return backgroundColor;
    }

    // Set for Background
    public void setBackgroundColor(Color backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    // Our Paint method which uses Graphics to paint
    @Override
    protected void paintComponent(Graphics g) {

        super.paintComponent(g);

        g.setColor(getBackgroundColor());
        g.fillRect(0, 0, getWidth(), getHeight());
    }
}



